/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch2;

import java.util.Objects;

/**
 *
 * @author AIR
 */
public final class CaseInsensitiveString {

    private final String s;

    public CaseInsensitiveString(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        this.s = s;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if(obj instanceof CaseInsensitiveString)
//            return s.equalsIgnoreCase(((CaseInsensitiveString)obj).s);
//        if(obj instanceof String){
//            return s.equalsIgnoreCase((String)obj);
//        }
//        return false;
//    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CaseInsensitiveString &&
                ((CaseInsensitiveString)obj).s.equalsIgnoreCase(s);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.s);
        return hash;
    }

    
}
