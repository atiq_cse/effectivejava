/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch2;

import java.awt.Color;

/**
 *
 * @author AIR
 */
public class ColorPoint extends Point{
    private final Color color;
    public ColorPoint(int x, int y,Color color) {
        super(x, y);
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Point))
            return false;
        return super.equals(obj) && ((ColorPoint)obj).color == color;
    }
    
    
    
}
