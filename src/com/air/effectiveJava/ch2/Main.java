/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch2;

import java.awt.Color;

/**
 *
 * @author AIR
 */
public class Main {

    public static void main(String[] args) {
//        CaseInsensitiveString cis = new CaseInsensitiveString("Polish");
//        String s = "polish";
//        System.out.println(cis.equals(s));
//        System.out.println(s.equals(cis));

        Point p = new Point(1, 2);
        ColorPoint cp = new ColorPoint(1, 2, Color.RED);
        System.out.println(p.equals(cp));
        System.out.println(cp.equals(p));
        
    }
}
