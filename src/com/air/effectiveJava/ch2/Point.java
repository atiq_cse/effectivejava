/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch2;

/**
 *
 * @author AIR
 */
public class Point {
    private final int x;
    private final int y;
    
    public Point(final int x,final int y ){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Point)) return false;
        Point point = (Point)obj;
        return point.x == x && point.y == y;
    }
    
    
}
