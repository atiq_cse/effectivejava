/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch3.item8;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AIR
 */
public class TestItem8 {
    public static void main(String[] args) {
        CaseInsensitiveString cis = new CaseInsensitiveString("Polish");
        String s = "polish";
        System.out.println(cis.equals(s));
        System.out.println(s.equals(cis));
        
        List<CaseInsensitiveString> list = new ArrayList<CaseInsensitiveString>();
        list.add(cis);
        System.out.println(list.contains(s));
        System.out.println(list.contains(cis));
    }
}
