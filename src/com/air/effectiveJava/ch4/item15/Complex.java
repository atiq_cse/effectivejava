/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch4.item15;

/**
 *
 * @author AIR
 */
public final class Complex {
    private final double re;
    private final double im;

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }
    
    public Complex add(Complex c){
        return new Complex(re+c.re,im+c.im);
    }
    
    public Complex subtract(Complex c){
        return new Complex(re-c.re,im-c.im);
    }
    
    public Complex multiply(Complex c){
        return new Complex(re*c.re - im*c.im,re*c.im + im*c.re);
    }

    @Override
    public String toString() {
        return "("+re+" + "+im+"i)";
    }
    
    
}
