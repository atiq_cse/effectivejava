/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch7.item39; 

import java.util.Date;

/**
 *
 * @author AIR
 */
public class Main {
    public static void main(String[] args) {
        Date start = new Date();
        Date end = new Date();
        Period p = new Period(start, end);
        System.out.println(p.getStart()+" "+p.getEnd());
        end.setYear(88);
        System.out.println(p.getStart()+" "+p.getEnd());
    }
    
}
