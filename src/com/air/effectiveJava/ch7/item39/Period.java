/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.air.effectiveJava.ch7.item39;

import java.util.Date;

/**
 *
 * @author AIR
 */
public final class Period {
    private final Date start;
    private final Date end;

    public Period(Date start, Date end) {
        this.start = new Date(start.getTime());
        this.end = new Date(end.getTime());
        if(start.compareTo(end) > 0){
            throw new IllegalStateException(start+" after "+end);
        }        
    }

    public Date getStart() {
        return (Date) start.clone();
    }

    public Date getEnd() {
        return (Date) end.clone();
    }
    
    
}
